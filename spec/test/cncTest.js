var Page = require('../../lib/pageFactory');
import { Selector } from 'testcafe';

fixture('Validating cnc pages')
  .page('https://www.loblaws.ca/');

test('Print on console the price for each product displayed in default viewport', async (t) => {
    await Page.Home.printPrice(t);

});

test('Print on console the price for each product with estimated price (.est) without $', async (t) => {
    var arr =await Page.Home.getESTPriceList(t);
    console.log(`Values of EST price without $ as : ${arr}`)

});


test('Navigate to PDP for the product with lowest estimated price (.est)', async (t) => {
    var arr =await Page.Home.getESTPriceList(t);
    var lowestESTPriceValue = Math.min.apply(Math,arr);
    await Page.Home.navigatePDPWithESTPrice(t, lowestESTPriceValue);
});