import {Selector} from 'testcafe'

class HomePage {
    constructor() {
        this.productRow     = Selector('.visible-lg');
        this.products       = '.quickview-simple';
        this.productPrice   = 'data-product-price';
        this.estPriceTag    = '.estimated-price';
        this.productName    = '.js-product-entry-name';
        this.estPrice       = Selector('.estimated-price');
        this.productName    = '.js-product-entry-name';
        this.activeProducts = '.owl-item.active';
    }

    async removePriceTag(price) {
        var str = price;
        var updateStr = str.replace("$", "");
        return updateStr;
    }

    async printPrice(t) {
        await t.wait(1000);//took time to render
        const productCount = await (this.productRow.find(this.activeProducts)).count;
        for (let i = 0; i < productCount; i++) {
            const value = await this.productRow.find(this.activeProducts).nth(i).find(this.products).getAttribute(this.productPrice)
            console.log(`Price for product ${i+1} is ${value}`);
        }

    }

    async getESTPriceList(t){
        await t.wait(1000);//took time to render
        let estPriceList = []; 
        const estProduct = await (this.estPrice.parent(this.products));
        const estProductCount = await estProduct.count;
        for (let i = 0; i < estProductCount; i++) {
            if(await estProduct.nth(i).visible) {
                const value = await (await estProduct.nth(i)).getAttribute(this.productPrice);
                estPriceList.push(parseFloat(await this.removePriceTag(value)));         
            }
        }
        return estPriceList;
    }


    async navigatePDPWithESTPrice(t, lowestESTValue) {
        await t.wait(1000);
        const estProduct = await (this.estPrice.parent(this.products));
        const estProductCount = await estProduct.count;
        for (let i = 0; i < estProductCount; i++) {
            if(await estProduct.nth(i).visible) {
               const value = await (await estProduct.nth(i)).getAttribute(this.productPrice);
               const priceValue = parseFloat(await this.removePriceTag(value));   
               if(priceValue === lowestESTValue) {
                    await t.click(estProduct.nth(i).find(this.productName));
                    break;
                }
            }
        }
    }

}
module.exports = HomePage;